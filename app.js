//const fs = require('fs');
const Koa = require('koa');
//const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const controller = require('./controller');
const app = new Koa();
app.use(async (ctx, next) => {
  console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
  await next();
});
//koa-bodyparser必须在router之前被注册到app对象上
 app.use(bodyParser());
 app.use(controller());
 app.listen(3000);
console.log('app started at port http://localhost:3000');
/* for(let f of js_files){
  console.log(`process controller:${f}`);
  //导入js文件
  let mapping=require(__dirname+'/controllers/'+f);
  for(let url in mapping){
    if(url.startsWith('GET')){
      // 如果url类似"GET xxx":
      let path=url.substring(4);
      router.get(path,mapping[url]);
      console.log(`register URL mapping: GET ${path}`);
    }else if(url.startsWith('POST')){
      let path=url.substring(5);
      router.post(path, mapping[url]);
      console.log(`register URL mapping: POST ${path}`);
    }else{
      console.log(`invalid URL: ${url}`);
    }
  }
}



let time=async (ctx,next)=>{
  const start=new Date().getTime();//当前时间
   await next();
  const ms=new Date().getTime()-start;//耗费时间
  console.log(`Time:${ms}ms`);
}
let html=async (ctx,next)=>{
  await next();
  ctx.response.type='text/html';
  ctx.response.body=`<h1>Hello,koa2!</h1>`;

}
app.use(cons);
app.use(time);
app.use(html);
let url=async (ctx,next)=>{
  console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
  await next();
}
router.get('/hello/:name',async (ctx,next)=>{
  let name=ctx.params.name;
  ctx.body=`<h1>Hello,${name}</h1>`;
});
router.get('/',async(ctx,next)=>{
  ctx.body=`
  <h1>Index</h1>
  <form action="/signin" method="post">
    <p>Name:<input name="name" value="koa"/></p>
    <p>Password:<input name="password" type="password"/></p>
    <p><input  type="submit" vlaue="submit"/></p>
  </form>
  `
});
router.post('/signin',async(ctx,next)=>{
  let 
    name=ctx.request.body.name || '',
    password=ctx.request.body.password || '';
    console.log(`signin with name:${name},password:${password}`);
    if(name==='loa' && password==='12345'){
      ctx.body=`<h1>Welcome,${name}!</h1>`;
    }else{
      ctx.body = `<h1>Login failed!</h1>
      <p><a href="/">Try again</a></p>`;
    }
})

//koa-bodyparser必须在router之前被注册到app对象上
app.use(bodyParser());
//添加路由中间件
app.use(router.routes());
app.listen(3000);
console.log('app started at port http://localhost:3000'); */